with open("Day8_input.txt", "r") as f:
    content = [i.split("|") for i in f.read().split("\n")]

digits = []
digCount = {}
for i in content:   digits.append([i[0].split(), i[1].split()])

for lst in digits:
    for num in lst[1]:
        try:
            digCount[len(num)] += 1
        except KeyError:
            digCount[len(num)] = 1
            
print(sum([digCount[number] for number in [2,3,4,7] if number in digCount]))