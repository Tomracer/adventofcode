with open("Day2_input.txt", "r") as f:
    content = f.read()
    steps = [int(i) for i in content.split() if i.isdigit()]
    direction = [i for i in content.split() if not i.isdigit()]

# Part 1
def part1(direction, count):
    depth, horizontal = 0, 0
    for key, x in enumerate(direction):
        if x == "forward": horizontal += count[key]
        elif x == "down": depth += count[key]
        elif x == "up": depth -= count[key]
    return depth*horizontal

# Part 2
def part2(direction, count):
    aim, horizontal, depth = 0, 0, 0
    for key, x in enumerate(direction):
        if x == "down": aim += count[key]
        elif x == "up": aim -= count[key]
        elif x == "forward":
            horizontal += count[key]
            depth += aim*count[key]
    return(horizontal*depth)


print(f"Part 1:  {part1(direction, steps)}\nPart 2: {part2(direction, steps)}")
