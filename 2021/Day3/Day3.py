with open("Day3_input.txt", "r") as f:
    content = f.read().split("\n")

def part1(string):
    gamma, epsilon = "", ""
    for i in range(len(string[0])):
        for value in string:
            zero, one = 0, 0
            if value[i:i+1] == "1": one += 1
            elif value[i:i+1] == "0": zero += 1

        if one > zero: gamma += "1";epsilon += "0"
        if one < zero: gamma += "0"; epsilon += "1"
    return f"Part 1:\n  -Gamma: {int(gamma, 2)}\n  -Epsilon: {int(epsilon, 2)}\n  -Power consumption: {int(gamma, 2)*int(epsilon, 2)}"

print(part1(content))

def part2(string, setting):
    index = 0
    while len(string) > 1:
        zero = 0
        one = 0
        zeros = []
        ones = []
        for value in string:
            if value[index:index+1] == "0":
                zero +=1
                zeros.append(value)
            elif value[index:index+1] == "1":
                one += 1
                ones.append(value)
        if setting == 0:
            if one > zero:
                string = zeros
            elif zero > one:
                string = ones
        elif setting == 1:
            if one < zero:
                string = zeros
            else:
                string = ones
        index += 1
        
    return int(string[0], 2)


print(f"Part 2:\n  -Oxygen: {part2(content, 1)}\n  -CO²: {part2(content, 0)}\n  -CO² scrubber rating: {part2(content, 0)*part2(content, 1)}")