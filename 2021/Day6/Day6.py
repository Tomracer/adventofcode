with open("Day6_input.txt", "r") as f:
    content = f.read().split(",")

def fish(values, days):
    fishes = {1:{"day":0, "number":0}, 2:{"day":1, "number":0}, 3:{"day":2, "number":0}, 4:{"day":3, "number":0}, 5:{"day":4, "number":0} , 6:{"day":5, "number":0}, 7:{"day":6, "number":0}, 8:{"day":7, "number":0}, 9:{"day":8, "number":0}}
    for f in values:  fishes[int(f)+1]["number"] += 1
    newfish = fish6 = 0
    #fish6 = 0
    for x in range(days):
        for i in range(1,len(fishes)+1):
            if fishes[i]["day"]-1 == -1:
                newfish = fishes[i]["number"]
                fish6 = fishes[i]["number"]
                fishes[i]["number"] = 0
            else:
                fishes[i-1]["number"] = fishes[i]["number"]
                fishes[i]["number"] = 0
        fishes[9]["number"] = newfish
        fishes[7]["number"] += fish6
    return fishes

Part1, Part2 = sum([i["number"] for i in fish(content, 80).values()]), sum([i["number"] for i in fish(content, 256).values()])
print(f"Part 1: {Part1}\nPart 2: {Part2}")