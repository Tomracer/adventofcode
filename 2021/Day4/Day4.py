with open("Day4_input.txt", "r") as f:
    content = f.read()
    numbers = content.split(",")
    numbers[-1] = numbers[-1][0:1]
    cards = content.split("\n")[2:]

cardTable, cardTableMain = {}, {}
index, index2 = 1, 1
known = []

# Generate Data structure
for key, card in enumerate(cards):
    if card == "":
        cardTableMain[index] = cardTable
        cardTable = {}
        index +=1
        index2 = 1
        continue
    cardTable[index2] = card.split()
    index2 += 1

# Selecting algorithm, you need to still search for the correct answer in the terminal output
for number in numbers:
    for card in range(1,len(cardTableMain)+1):
        for line in range(1, len(cardTableMain[1])+1):
            for column in range(len(cardTableMain[1])):
                if cardTableMain[card][line][column].isdigit() and int(cardTableMain[card][line][column]) == int(number) and card not in known:
                    #print(f"card {card} line {line} column {column}: ", number)
                    cardTableMain[card][line][column] = "#"
                    if cardTableMain[card][line][0] == "#" and cardTableMain[card][line][1] == "#" and cardTableMain[card][line][2] == "#" and cardTableMain[card][line][3] == "#" and cardTableMain[card][line][4] == "#":
                        print(f"Card {card} won with index number {numbers.index(number)} in line {line}.\n  -Number: {number}")
                        for i in range(1, 6):
                            print(f"  {cardTableMain[card][i]}")
                        known.append(card)
                    elif cardTableMain[card][1][column] == "#" and cardTableMain[card][2][column] == "#" and cardTableMain[card][3][column] == "#" and cardTableMain[card][4][column] == "#" and cardTableMain[card][5][column] == "#":
                        print(f"Card {card} won with index number {numbers.index(number)} in column {column}.\n  -Number: {number}")
                        for i in range(1, 6):
                            print(f"  {cardTableMain[card][i]}")
                            known.append(card)