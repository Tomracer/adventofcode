with open("Day5_input.txt", "r") as f:
    content = f.read()
    l = content.split("\n")

def straight(p1,p2, Coordinats):
    r1, r2, p = (min(p2[0], p1[0]), max(p2[0], p1[0]), p2[1]) if p2[1] - p1[1] == 0 else (min(p2[1], p1[1]), max(p2[1], p1[1]), p2[0])
    for coordinate in range(r1, r2+1):
        try:
            if (p == p1[1]) and (p == p2[1]):
                Coordinats[(coordinate, p)] += 1
            else:
                Coordinats[(p, coordinate)] += 1
        except KeyError:
            if (p == p1[1]) and (p == p2[1]):
                Coordinats[(coordinate, p)] = 1 
            else:
                Coordinats[(p, coordinate)] = 1
    return Coordinats

def part1(string):
    Coordinats = {}
    for value in string:
        try:
            p1 = (int(value.split(" -> ")[0].split(",")[0]), int(value.split(" -> ")[0].split(",")[1]))
            p2 = (int(value.split(" -> ")[1].split(",")[0]), int(value.split(" -> ")[1].split(",")[1]))
        except ValueError: continue
        if p1[0] == p2[0]:
            Coordinats = straight(p1, p2, Coordinats)
        elif p1[1] == p2[1]:
            Coordinats = straight(p1, p2, Coordinats)
    return Coordinats

def part2(string):
    Coordinats = {} 
    for value in string:
        try: 
            p1 = (int(value.split(" -> ")[0].split(",")[0]), int(value.split(" -> ")[0].split(",")[1]))
            p2 = (int(value.split(" -> ")[1].split(",")[0]), int(value.split(" -> ")[1].split(",")[1]))
        except ValueError: continue
        try:
            m = (p2[1] - p1[1]) // (p2[0] - p1[0])
            if m == 0: Coordinats = straight(p1, p2, Coordinats);continue
            # f(x)=mx+b - Generate the functions
            side1 = p1[1]
            side2 = m*p1[0]#+b
            if side1 == 0: b = (side2 // -1)
            elif side2 == 0: b = (side1 // -1)
            else:
                side2 += (side1 // -1)
                b = side2 // -1
            for x in range(min(p1[0], p2[0]), max(p1[0], p2[0])+1):
                    try:    
                        Coordinats[(x, (m*x)+b)] += 1
                    except KeyError:    
                        Coordinats[(x, (m*x)+b)] = 1

            #print(f"f(x)={m}*x+{b}")
        except ZeroDivisionError: 
            Coordinats = straight(p1, p2, Coordinats)
    return Coordinats

print(f"Part 1: {sum([1 for i in part1(l).values() if i >= 2])}")
print(f"Part 2: {sum([1 for i in part2(l).values() if i >= 2])}")