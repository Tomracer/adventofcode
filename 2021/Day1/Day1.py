with open("Day1_input.txt", "r") as f:
  content = [int(i) for i in f.read().split("\n")]
  
# Part 1
def part1(measurements):
  increased, current = 0, 0
  for num in measurements:
    if current == 0: current = num; continue
    elif num > current: increased +=1; current = num; continue
    else: current = num
  return f"Part 1:\n  -Increased {increased} times"

# Part 2
def part2(measurements):
  increased, current, pos = 0, 0, 0
  key = 0
  len_ = len(measurements) // 3
  stop = False
  while not stop:
    try:
      if current == 0:
        current = measurements[key] + measurements[key+1] + measurements[key+2]
      elif current > (measurements[key] + measurements[key+1] + measurements[key+2]):
        current = measurements[key] + measurements[key+1] + measurements[key+2]
      elif current < (measurements[key] + measurements[key+1] + measurements[key+2]):
        increased += 1
        current = measurements[key] + measurements[key+1] + measurements[key+2]
    except IndexError:
      stop = True

    key +=1
  return f"Part 2:\n  -Increased {increased} times"


print(part1(content))
print(part2(content))