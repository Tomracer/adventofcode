# AdventOfCode

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

https://adventofcode.com

## Content

In this repository i store the solutions for the respective tasks of the adventofcode calendar days.

As I participated this year for the first time in the AdvenOfCode, here are only solutions to some 
of the tasks from the year 2021.

## License
MIT


